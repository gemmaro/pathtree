(use-modules ((guix licenses)
              #:prefix license:)
             (gnu packages ruby)
             (guix packages)
             (guix build-system ruby)
             (guix git-download))

;; TODO: submit to Guix official channel
(define-public ruby-rubocop-minitest
  (package
    (name "ruby-rubocop-minitest")
    (version "0.30.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/rubocop/rubocop-minitest")
                    (commit "30a7d4e83d392f3da05c16d5e5fa589e71833bc1")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1957jvhqpz4q48b8ma3kfzh4585gfx9p9j4lhfkdk06mwyqpqzjx"))))
    (build-system ruby-build-system)
    (arguments
     `(#:phases (modify-phases %standard-phases
                  (add-before 'check 'set-home-dir
                    (lambda _
                      (setenv "HOME"
                              (getcwd)))))
       #:tests? #f))
    (native-inputs (list ruby-bump ruby-yard))
    (propagated-inputs (list ruby-rubocop))
    (synopsis "Automatic Minitest code style checking tool")
    (description
     "Automatic Minitest code style checking tool.  A RuboCop extension
focused on enforcing Minitest best practices and coding conventions.")
    (home-page "https://docs.rubocop.org/rubocop-minitest/")
    (license license:expat)))

(packages->manifest (list ruby-rubocop
                          ruby-rubocop-minitest
                          ruby-rubocop-rake
                          ruby-rubocop-performance
                          ruby-simplecov
                          ruby
                          bundler))
