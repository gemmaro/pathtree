require 'test_helper'

module Pathtree
  class DslTest < Minitest::Test
    def test_module
      refute_respond_to Pathtree::Dsl, :new
    end

    class FileTest < Minitest::Test
      def test_file
        assert_equal Pathname('a'), Pathtree.load('file :a').a
        assert_equal Pathname('a.b'), Pathtree.load("file :a, 'a.b'").a
        assert_equal Pathname('A'), Pathtree.load('file :a, ->(f) { f.capitalize }').a
      end

      def test_root
        assert_equal Pathname('x') / 'a', Pathtree.load('file :a', root: 'x').a
      end

      def test_names
        assert_equal [:a], Pathtree.load('file :a').names
      end

      def test_duplicate
        assert_raises(Pathtree::MethodDuplicationError, 'a') do
          Pathtree.load(<<~END_OF_PATHTREE)
            file :a
            file :a
          END_OF_PATHTREE
        end
      end

      def test_duplicate_directory
        assert_raises(Pathtree::MethodDuplicationError, 'a') do
          Pathtree.load(<<~END_OF_PATHTREE)
            file :a
            directory :a
          END_OF_PATHTREE
        end
      end

      def test_pathname
        assert_equal Pathname('b'), Pathtree.load('file :a, Pathname("b")').a
      end
    end

    class DirectoryTest < Minitest::Test
      def test_directory
        assert_equal Pathname('a'), Pathtree.load('directory :a').a
        assert_equal Pathname('a-b'), Pathtree.load("directory :a, 'a-b'").a
      end

      def test_directory_nested
        assert_equal Pathname('a'), Pathtree.load(<<~END_OF_PATHTREE).a
          directory :a do
            file :b
          end
        END_OF_PATHTREE

        assert_equal Pathname('a') / 'b', Pathtree.load(<<~END_OF_PATHTREE).a.b
          directory :a do
            file :b
          end
        END_OF_PATHTREE
      end

      def test_directory_doubly_nested
        p = Pathtree.load(<<~END_OF_PATHTREE)
          directory :a do
              file :x

            directory :b do
              file :y
            end
          end
        END_OF_PATHTREE

        assert_equal Pathname('a') / 'x', p.a.x
        assert_equal Pathname('a') / 'b' / 'y', p.a.b.y
      end
    end

    def test_dir
      assert_equal Pathname('a'), Pathtree.load('dir :a').a
    end
  end
end
