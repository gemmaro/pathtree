require 'test_helper'

class PathtreeTest < Minitest::Test
  def test_module
    refute_respond_to Pathtree, :new
  end

  def test_version
    refute_nil ::Pathtree::VERSION
  end

  def test_load
    assert_respond_to Pathtree, :load
  end

  def test_load_root
    assert_equal Pathname('a'), Pathtree.load('', root: 'a')
  end

  def test_read
    assert_respond_to Pathtree, :read
    Pathtree.read("#{__dir__}/fixtures/sample.rb")
  end
end
