require 'pathname'

module Pathtree
  # DSL for constructing path tree
  module Dsl
    attr_reader :names

    # Grows file leaf from the branch;
    # The path at the point where this method is called
    # attempts to create an instance method that returns the path with the +name+.
    #
    # * If +path+ is +nil+, +name+ is used instead.
    # * If +path+ is a +Proc+, give +name+ to it.
    # * Otherwise, +path+ itself is used.
    def file(name, path = nil)
      check_name(name)
      path = normalize(path, name:).then { nest(_1) }
      sprout(name:, path:)
    end

    # Grows directory branch from the branch.
    # See also Pathtree::Dsl#file method.
    # This method has optional block, which represents nested file structure.
    def directory(name, path = nil, &block)
      check_name(name)
      path = normalize(path, name:).then { nest(_1) }
                                   .then { Dsl.extend(_1) }
                                   .tap { _1.instance_eval(&block) if block }
      sprout(name:, path:)
    end

    alias dir directory

    def self.extend(path)
      path.instance_variable_set(:@names, [])
      path.extend(Dsl)
      path
    end

    private

    def check_name(name)
      raise MethodDuplicationError, name if respond_to?(name)
    end

    def normalize(path, name:)
      if path.nil?
        Pathname(name.to_s)
      elsif path.is_a?(String)
        Pathname(path)
      elsif path.respond_to?(:call)
        path.call(name).to_s.then { Pathname(_1) }
      else
        path
      end
    end

    def nest(path)
      is_a?(Trunk) ? path : self / path
    end

    def sprout(name:, path:)
      define_singleton_method(name) { path }
      @names << name
    end
  end
end
