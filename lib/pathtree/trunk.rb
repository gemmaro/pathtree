require 'pathname'
require_relative 'dsl'

module Pathtree
  # A trunk of path tree
  class Trunk
    include Dsl

    def initialize
      @names = []
    end
  end
end
