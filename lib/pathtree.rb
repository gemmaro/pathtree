require_relative 'pathtree/version'
require_relative 'pathtree/trunk'

# Pathname DSL
module Pathtree
  class MethodDuplicationError < NameError; end

  # Read Pathtree DSL from +path+.
  # +root+ is base directory for tree.
  def self.read(path, root: nil)
    dsl = File.read(path)
    self.load(dsl, root:)
  end

  # Load Pathtree DSL source.
  # +root+ is base directory for tree.
  def self.load(dsl, root: nil)
    (root ? to_path(root) : Trunk.new).tap { _1.instance_eval(dsl) }
  end

  class << self
    private

    def to_path(value)
      value = Pathname(value) if value.is_a?(String)
      Dsl.extend(value)
    end
  end
end
