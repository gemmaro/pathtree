# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## 0.5.0

This is a maintenance release, but updated required Ruby version to 3.1.

* Fix example code.
* Update gemspec fields; especially fill in link metadata.
* Add draft signature (RBS).

## 0.4.2

* Added Guix manifest; `guix shell` to enter the dev env.
* Updated gem versions.
* Clean gemspec file.
* Updated RuboCop configs.

### Added in 0.4.2

* Support Ruby 2.7 or later.

## 0.4.1

### Fixed in 0.4.1

* It is now possible to define identically named paths in different hierarchies.
  In 0.4.0, duplicate names caused an exception.

```ruby
directory :aaa do
  file :xxx

  directory :bbb do
    file :xxx # OK
  end
end
```

## 0.4.0 - 2022-05-14

### Changed in 0.4.0

* Moved `Pathtree::Dsl.read` and `Pathtree::Dsl.load` to `Pathtree.read` and `Pathree.load`.
* If root path is not given, `Pathtree::Trunk` is used, rather than `Pathname.pwd`.

### Removed in 0.4.0

* Refinements for `String`, `Pathname`, and `Object`

## 0.3.0 - 2021-12-12

### Added in 0.3.0

* GitLab CI which checks tests with Minitest and lint rules with RuboCop
* Enable to pass `Proc` object to `path` argument
  of `Pathtree::Dsl#file` and `Pathtree::Dsl#directory`.

  ```ruby
  file :main_idr, -> { _1.capitalize } # Main.idr
  ```

* `Pathtree::StringRefinements#split_extension`
* `Pathtree::ObjectRefinements#file_path` and `Pathtree::ObjectRefinements#directory_path`

### Removed in 0.3.0

* `Pathtree::SymbolRefinements`
  * Move to `Pathtree::StringRefinements#split_extension`
* `dot` option for `Pathtree::Dsl#file` and `Pathtree::Dsl#directory`
  * Use `Proc` for path instead

### Changed in 0.3.0

* Visibility of `Pathtree::Dsl#read` to private

## 0.2.0 - 2021-11-20

### Changed in 0.2.0

* `Pathtree` inherits `Pathname`

`paths.rb`:

```ruby
dir :aaa
```

`main.rb`:

```ruby
# old (not works for now, see removed section)
Pathtree::Dsl.read('paths.rb').aaa.then { path(_1) } # $PWD/aaa

# new
Pathtree::Dsl.read('paths.rb').aaa # $PWD/aaa
```

### Removed in 0.2.0

* `KernelRefinements` since `Pathtree` directory object is now `Pathtree` itself
* Instance reader attribute `@tree` of `Pathtree::Dsl`

## 0.1.1 - 2021-11-20

### Added in 0.1.1

* `dot` option for `Pathtree::Dsl#file` and `Pathtree::Dsl#directory`.
  If `dot: true`, then path name is prefixed with dot (period) character
  unless it start with dot already.

```ruby
# old
file :rubocop_yml, '.rubocop_yml'

# new
file :rubocop_yml, dot: true # .rubocop.yml
```

* `Pathtree::Dsl#dir` alias for `Pathtree::Dsl#directory`.

## 0.1.0 - 2021-11-13

* Initial release
