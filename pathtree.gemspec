require_relative 'lib/pathtree/version'

Gem::Specification.new do |spec|
  name                       = 'pathtree'
  spec.name                  = name

  spec.version               = Pathtree::VERSION
  spec.authors               = ['gemmaro']
  spec.email                 = ['gemmaro.dev@gmail.com']

  spec.summary               = 'Pathname DSL'
  spec.description           = spec.summary
  spec.license               = 'MIT'
  spec.required_ruby_version = '>= 3.1'
  spec.files                 = Dir['*.{md,txt}', 'lib/**/*']
  spec.require_paths         = ['lib']

  homepage                   = 'https://gitlab.com/gemmaro/pathtree'
  spec.homepage              = homepage

  spec.metadata = {
    'rubygems_mfa_required' => 'true',
    'bug_tracker_uri' => "#{homepage}/-/issues",
    'changelog_uri' => "#{homepage}/-/blob/main/CHANGELOG.md",
    'documentation_uri' => "https://www.rubydoc.info/gems/#{name}",
    'homepage_uri' => homepage,
    'source_code_uri' => homepage,
    'wiki_uri' => "#{homepage}/-/wikis/home"
  }
end
