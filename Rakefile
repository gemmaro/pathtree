require 'bundler/gem_tasks'
require 'rake/testtask'

Rake::TestTask.new(:test) do |t|
  t.libs << 'test'
  t.libs << 'lib'
  t.test_files = FileList['test/**/*_test.rb']
end

require 'rubocop/rake_task'

RuboCop::RakeTask.new

require 'rdoc/task'

RDoc::Task.new do |rdoc|
  readme = 'README.md'

  rdoc.main = readme
  rdoc.rdoc_files.include(readme, 'lib/**/*')
end

task default: %i[test rubocop]

desc 'Lint Markdown documents'
task md: Dir['**/*.md'] do |t|
  sh "mdl #{t.sources.join(' ')}"
end

desc 'format Guix manifest'
task :fmt do
  sh 'guix style -f manifest.scm'
end

desc 'generate signature (RBS)'
task :gensig do
  sh 'typeprof', '-r', 'pathtree', '-o', 'sig/pathtree.gen.rbs', *Dir['lib/**/*.rb']
end
